import pytest

def add(a, b):
    return a + b
def setup_function():
    print('开始计算')
def teardown_function():
    print('结束计算')

@pytest.mark.hebeu
def test_hebeu():
    assert add(-99,99 ) == 0
    assert add(0,99 ) == 99
@pytest.mark.hebeu
def test_hebeu2():
    assert add(3,2 ) == 5
    assert add(-1.1,2.2 ) == 1.1
